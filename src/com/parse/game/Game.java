package com.parse.game;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.starter.GameSession;

public class Game {
    GameSession session; //ParseObject we modify, uses the network.
    String playerOne;
    String playerTwo;

    // Whether this game is open or closed
    String gameStatus;

    // Which player has to move next
    String playerTurn;

    // Indicates other player in the game that is not the current user.
    String otherPlayer;

    int numTotalImages;

    int numTurns;
    int framesPerTurn;

    // When uploading images we keep track of how many and signal the UI when done.
    int framesUploaded;

    // Checks if game is over
    String gameEnd;

    // Save byte data for images we download here.
    byte[] data = null;

    // Checks if this is an existing game or a new one.
    boolean firstTurn;

    // Saves images for only this user's turn. These are the ones we draw
    public ArrayList<Bitmap> sessionFrames;
    ArrayList<ParseFile> fileArray;

    // Saves images for the last turn... Currently we only save the last one drawn.
    // Could be changed to only one bitmap.
    ArrayList<Bitmap> previousTurnFrames;

    // temporary bitmap
    Bitmap bmp;

    public Game(int framesPerTurn, int numTurns, String p1, String p2, String status) {
        this.framesPerTurn = framesPerTurn;
        this.numTurns = numTurns;
        playerOne = p1;
        playerTwo = p2;
        gameStatus = status;
        sessionFrames = new ArrayList<Bitmap>();
        previousTurnFrames = new ArrayList<Bitmap>();
        fileArray = new ArrayList<ParseFile>();
        bmp.createBitmap(100, 200, Bitmap.Config.ARGB_8888);
        session = new GameSession();
    }

    public Game(GameSession gs) {
        session = gs;
        sessionFrames = new ArrayList<Bitmap>();
        previousTurnFrames = new ArrayList<Bitmap>();
        fileArray = new ArrayList<ParseFile>();
        bmp.createBitmap(100, 200, Bitmap.Config.ARGB_8888);
        numTurns = gs.getNumTurns();
    }

    public void setFirstTurn(boolean b) {
        firstTurn = b;
    }

    public void loadGame(Handler handler) {
        Log.d("gameState", "numTurns is " + numTurns);
        if (!firstTurn && numTurns != 0) {
            // Load metadata
            this.framesPerTurn = session.getFramesPerTurn();
            this.numTurns = session.getNumTurns();
            playerOne = session.getPlayerOne();
            playerTwo = session.getPlayerTwo();
            gameStatus = session.getGameStatus();
            numTotalImages = session.getTotalImagesSaved();
            loadImage(handler); // startGame is called after downloading images.
        }

        else if (numTurns == 0) {
            Log.d("gameState", "Loading Game: numTurns is 0");
            this.framesPerTurn = session.getFramesPerTurn();
            this.numTurns = session.getNumTurns();
            playerOne = session.getPlayerOne();
            playerTwo = session.getPlayerTwo();
            gameStatus = session.getGameStatus();
            numTotalImages = session.getTotalImagesSaved();
            startGame(handler);
        }

        // First time
        else {
            Log.d("game", "loading new game");
            // Assert playerOne is the current User
            // Assert numTurns and framesPerTurn are valid
            gameStatus = "closed";
            startGame(handler);
        }

    }

    /** Loads last image from the last turn **/
    private void loadImage(final Handler handler) {
        // Gets images from the last session by specifying correct indices in our file array.
        // We just need one image but for now we'll keep this array in case we want to download more
        // than one.
        ArrayList<ParseFile> imageArray = new ArrayList<ParseFile>();
        imageArray = session.getTurnImageFiles(numTotalImages - framesPerTurn, numTotalImages);

        // assert framesPerTurn == imageArray.size();
        imageArray.get(framesPerTurn - 1).getDataInBackground(new GetDataCallback() {
            public void done(byte[] d, ParseException e) {
                if (e == null) {
                    // data has the bytes for the image
                    data = d;
                    bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                    previousTurnFrames.add(bmp);
                    startGame(handler);
                } else {
                    Log.d("parseNetwork", "failure to download image");
                }
            }
        });
    }

    private void startGame(Handler handler) {
        // Tell UI thread we are ready to start the game
        Message msg = new Message();
        msg.what = 1;
        handler.sendMessage(msg);
    }

    public void clearData() {
        sessionFrames.clear();
        previousTurnFrames.clear();
    }

    public boolean isOver() {
        return numTurns <= 0;
    }

    public boolean isFirstTurn() {
        return firstTurn;
    }

    public void endTurn(Handler handler) {

        --numTurns;
        otherPlayer = "";
        // Update whose turn it is next
        String user = ParseUser.getCurrentUser().getUsername();
        Log.d("gameState", "current: " + user + " p1 " + playerOne + " p2" + playerTwo);
        if (playerOne.equals(user)) {
            otherPlayer = playerTwo;
        } else {
            otherPlayer = playerOne;
        }
        Log.d("gameState", "otherplayer: " + otherPlayer);
        session.setPlayerTurn(otherPlayer);
        session.saveInBackground();

        // TODO Put loading screen here
        uploadImages(handler);
        Log.d("task", " upload complete");
        // Update GameState

        numTotalImages += framesPerTurn;
    }

    /**
     * Uploads the images to the database, but doesn't associate them To the
     * parseObject
     **/
    private void uploadImages(final Handler handler) {
        framesUploaded = 0;
        // Convert bitmaps drawn into ParseFile arrays

        for (int i = 0; i < sessionFrames.size(); i++) {

            // Compress into PNG
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            boolean compress = sessionFrames.get(i).compress(Bitmap.CompressFormat.PNG, 60, stream);
            if (compress)
                Log.d("parseNetwork", "compression success");
            else
                Log.d("parseNetwork", "compression failed");

            // Convert bytes into ParseFile
            final ParseFile file = new ParseFile("img.png", stream.toByteArray());
            // Save each file to the database
            file.saveInBackground(new SaveCallback() {
                public void done(ParseException e) {
                    if (e == null) {
                        fileArray.add(file);
                        Log.d("parseNetwork", "saved file to the cloud");
                        framesUploaded++;
                        // Save game when all images are in the database
                        if (framesUploaded == framesPerTurn) {
                            saveGame(handler);
                            Message msg = new Message();
                            msg.what = 1;
                            handler.sendMessage(msg);
                            clearData();
                        }
                    } else {
                        // error
                    }
                }
            });
        } // end for

    }

    /**
     * Links the images we uploaded to our parseObject by naming them correctly.
     **/
    public void saveGame(final Handler handler) {
        session.setFramesPerTurn(framesPerTurn);
        session.setNumTurns(numTurns);
        session.setTotalImagesSaved(numTotalImages);
        session.setPlayerOne(playerOne);
        session.setPlayerTwo(playerTwo);
        session.setGameStatus(gameStatus);

        //  Starts naming at the specified index = numTotalImages, number which doesn't include this session's images
        session.setTurnImageFiles(numTotalImages - framesPerTurn, fileArray);
        session.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("parseNetwork", "save game session success");
                    // Send message to other player through a unique channel number
                    ParsePush push = new ParsePush();
                    String message = "User " + ParseUser.getCurrentUser().getUsername()
                            + " made a move. Your turn!" + " Id: " + session.getObjectId();
                    push.setChannel(otherPlayer);
                    push.setMessage(message);
                    push.sendInBackground();

                } else {
                    Message msg = new Message();
                    msg.what = -1; // -1 will mean error
                    handler.sendMessage(msg);
                    Log.d("parseNetwork", "Could not save game");
                }
            }
        });
    }

    public void endGame() {
        clearData(); //clear frames
        System.gc();
    }

    public void downloadAllImages(Handler handler) {
        double progressStatus = 0;
        ArrayList<ParseFile> imageArray = new ArrayList<ParseFile>();

        imageArray = session.getImageFiles(numTotalImages);
        Log.d("parseNetwork", "number of files: " + imageArray.size());

        // assert framesPerTurn * numTurns == imageArray.size();

        // Get the data for each image of the game session and save it in the global frameArray
        for (int j = 0; j < numTotalImages; j++) {
            try {
                byte[] data = imageArray.get(j).getData();
                bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                Log.d("parseNetwork", "Downloaded image " + j);
                sessionFrames.add(bmp);

                // Update progressbar
                progressStatus++;
                double percent = (progressStatus / (double) numTotalImages) * 100;
                Message msg = new Message();
                msg.what = (int) percent;
                handler.sendMessage(msg);

            } catch (ParseException e) {
                Log.d("parseNetwork", "Download image failure");
                e.printStackTrace();
            }
        } // end all frame downloads
    }

    public int getFramesPerTurn() {
        return framesPerTurn;
    }

    public int getNumTurns() {
        return numTurns;
    }

    public String getPlayerOne() {
        return playerOne;
    }

    public String getPlayerTwo() {
        return playerTwo;
    }

    public int getTotalImagesSaved() {
        return numTotalImages;
    }

    public void addFrame(Bitmap b) {
        sessionFrames.add(b);
    }

    public void clearSessionFrames() {
        sessionFrames.removeAll(sessionFrames);
    }

    // Gets last frame from previous session, if any
    public Bitmap getLastTurnFrame() {
        return previousTurnFrames.get(previousTurnFrames.size() - 1);
    }

    public boolean isContinuedGame() {
        return previousTurnFrames.size() > 0;

    }

    public int getNumFramesDrawn() {
        return sessionFrames.size();
    }

}
