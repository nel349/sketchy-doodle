package com.parse.starter;


/** 
 * Taken from http://stackoverflow.com/questions/8593615/what-is-the-preferred-method-to-display-large-pre-rendered-animations-in-android
 **/

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;



public class ViewingBoard extends View {
    public Bitmap  mBitmap;
    public Canvas  mCanvas;
    private Path    mPath;
    private Paint   mBitmapPaint;
    private Paint   mPaint;
    
    private static final int DELAY = 140; //delay between frames in milliseconds
    private int play_frame = 0;
    private long last_tick = 0;
    private boolean mIsPlaying = false;
    private boolean mStartPlaying = true;
    

    float scaleX;
    float scaleY;
    int width;
    int height;
    int frameBufferWidth;
    int frameBufferHeight; 

    public ViewingBoard(Context c, AttributeSet attrs) {
        super(c, attrs);

        mPath = new Path();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(0xFF000000);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(9);
        
        // Smooth lines
        mPaint.setFilterBitmap(true);
     
        // Scale window for all devices
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);

        height = metrics.heightPixels;
        width = metrics.widthPixels;
        
        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        frameBufferWidth = isPortrait ? 480 : 800;
        frameBufferHeight = isPortrait ? 800 : 480;
        scaleX = (float) frameBufferWidth
                / width;
        scaleY = (float) frameBufferHeight
                / height;
        
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        

    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.scale((float)width / 480.0f, (float)height / 800.0f);
        if(play_frame == DispatchActivity.game.getNumFramesDrawn())
        {
            play_frame = 0;
        }
        if (mStartPlaying)
        {
            //Log.d(TAG, "starting animation...");
            play_frame = 0;
            mStartPlaying = false;
            mIsPlaying = true;
            postInvalidate();
        }
        else if (mIsPlaying)
        {
            
            if (play_frame >= DispatchActivity.game.getNumFramesDrawn())
            {
                mIsPlaying = false;
            }
            else
            {
                long time = (System.currentTimeMillis() - last_tick);
                int draw_x = 0;
                int draw_y = 0;
                if (time >= DELAY) //the delay time has passed. set next frame
                {
                    last_tick = System.currentTimeMillis();
                    canvas.drawBitmap(DispatchActivity.game.sessionFrames.get(play_frame), draw_x, draw_y, mPaint);
                    play_frame++;
                    postInvalidate();
                }
                else //still within delay.  redraw current frame
                {
                    canvas.drawBitmap(DispatchActivity.game.sessionFrames.get(play_frame), draw_x, draw_y, mPaint);
                    postInvalidate();
                }
            }
        }   
    }
    private void touch_up() {
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }


    public void clear(){
        mBitmap.eraseColor(Color.GREEN);
        invalidate();
        System.gc();
    }
}