package com.parse.starter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;



/** 
 * This DrawingBoard was put together with some sort of magic.
 * Android Paths get converted into my own paths made out of "points", which are
 * used and modified in certain calculations to create effects (see the Save Button)
 * 
 **/
public class DrawingBoard extends View {

    // Temporary frame data will be saved in bitmapCurrent before saving it into the framesArray
    public Bitmap mBitmap, bitmapCurrent;
    public Canvas mCanvas;
    private Path mPath;
    private Paint mPaint, mPaintTransparent, textPaint;
    private boolean clear;
    private boolean eraser = false;
    private boolean nextFrame = false;

    private boolean save;

    // Stores the paths we draw as Points so we can do stuff with them
    ArrayList<Point> points;
    
    // Stores all point Paths
    ArrayList<ArrayList<Point>> pointPaths;

    // Stores all android paths, which are drawn into the canvas.
    ArrayList<Path> pathsArray;

    // Stores previous frame's paths. These are to be drawn transparently
    ArrayList<Path> oldPathsArray;

    float scaleX;
    float scaleY;
    int width;
    int height;
    int frameBufferWidth;
    int frameBufferHeight;

    boolean firstTime = true;

    public DrawingBoard(Context c, AttributeSet attrs) {
        super(c, attrs);
        mPath = new Path();
        pathsArray = new ArrayList<Path>();
        Path p = new Path(mPath);
        pathsArray.add(p);

        oldPathsArray = new ArrayList<Path>();
        points = new ArrayList<Point>();
        pointPaths = new ArrayList<ArrayList<Point>>();

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(0xFF000000);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(5);

        mPaintTransparent = new Paint(mPaint);
        mPaintTransparent.setAlpha(50);

        textPaint = new Paint();
        textPaint.setTextSize(40);

        mCanvas = new Canvas();

        nextFrame = false;
        clear = false;
        save = false;

        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);

        height = metrics.heightPixels;
        width = metrics.widthPixels;

        // Scale the canvas for all devices based on the screen dimensions
        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        frameBufferWidth = isPortrait ? 480 : 800;
        frameBufferHeight = isPortrait ? 800 : 480;
        scaleX = (float) frameBufferWidth / width;
        scaleY = (float) frameBufferHeight / height;

    }

    /** When orientation changes, we keep the canvas in the same dimensions **/
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        frameBufferWidth = isPortrait ? 480 : 800;
        frameBufferHeight = isPortrait ? 800 : 480;

        mBitmap = Bitmap.createBitmap(frameBufferWidth, frameBufferHeight, Bitmap.Config.ARGB_8888);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!nextFrame)
            canvas.scale((float) width / 480.0f, (float) height / 800.0f);

        // Draw previous session's bitmap in transparent
        if (firstTime && DispatchActivity.game.isContinuedGame()) {
            //canvas.drawColor(Color.CYAN);
            canvas.drawBitmap(
                    DispatchActivity.game.getLastTurnFrame(), 0, 0,
                    mPaintTransparent);
        }

        if (eraser) {
            // mPaint.setColor(Color.WHITE);
            mPaint.setStrokeWidth(20);
        } else {
            mPaint.setColor(0xFF000000);
            mPaint.setStrokeWidth(5);
        }

        // If user just pressed "next frame"
        // We remove all transparent paths from canvas temporarily, then a snapshot is taken,
        // finally we set the current paths to transparent in the nextFrame() method.
        if (nextFrame) // remove transparent paths, set current paths to transparent
        {
            oldPathsArray.removeAll(oldPathsArray);
            for (int i = 0; i < pathsArray.size(); i++) { 
                oldPathsArray.add(pathsArray.get(i));
            }
            pathsArray.removeAll(pathsArray);
            pointPaths.removeAll(pointPaths);
            
            // Some weird voodoo here. If we don't add the mPath stuff isn't drawn in real time.
            pathsArray.add(mPath);
            
        }

        else { // draw current paths normally, draw old paths with transparent paint
            for (int i = 0; i < pathsArray.size(); i++) {
                canvas.drawPath(pathsArray.get(i), mPaint);
                canvas.drawPath(mPath, mPaint);
            }
            Log.d("pathSize", "old array:" + Integer.toString(oldPathsArray.size())
                    + " currentArray " + Integer.toString(pathsArray.size()));
            for (int j = 0; j < oldPathsArray.size(); j++) {
                canvas.drawPath(oldPathsArray.get(j), mPaintTransparent);
            }
        }
        
        // Don't even question why this is here and not in the nextFrame() method...
        // It only works this way so far...
        nextFrame = false;
    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 2;

    private void touch_start(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;

        Point point = new Point(x, y);
        points.add(point);
    }

    private void touch_move(float x, float y) {
        Point point = new Point(x, y);

        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        point.dx = x - mX;
        point.dy = y - mY;
        points.add(point);

        float deltaX = x - mX;
        float deltaY = y - mY;
        if (dx + dy > 10)
            Log.d("tolerance", Integer.toString((int) dx) + "    " + Integer.toString((int) dy));
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    private void touch_up() {
        mPath.lineTo(mX, mY);

        // If we create a new path and add it to the path array
        // instead of using mPath, it DOESNT work in Nexus 4, Nexus S 4G for some reason..
        // But has been fixed.
        //Path p = new Path(mPath);
        pathsArray.add(mPath);

        mPath = new Path();

        Log.d("save", "TOUCH UP");
        // save these pointPath into the array
        pointPaths.add(new ArrayList<Point>(points));
        points.clear();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX() * scaleX;
        float y = event.getY() * scaleY;
        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
            touch_start(x, y);
            invalidate();
            break;
        case MotionEvent.ACTION_MOVE:
            touch_move(x, y);
            invalidate();
            break;
        case MotionEvent.ACTION_UP:
            touch_up();
            invalidate();
            break;
        }
        return true;
    }

    public void save() {
        save = true;
        firstTime = false;
        // Log.d("save", "point issss" + Float.toString((pointPaths.get(0).get(10)).x));
        // Create a deep copy of all our original points we have drawn
        ArrayList<ArrayList<Point>> pointsCopy = new ArrayList<ArrayList<Point>>();
        for (int i = 0; i < pointPaths.size(); i++) {
            ArrayList<Point> t = new ArrayList<Point>();

            for (int j = 0; j < pointPaths.get(i).size(); j++) {
                Point a = new Point(pointPaths.get(i).get(j));
                t.add(a);
            }
            // Jitter these points
            pointsCopy.add(jitterPoints(t));
        }

        // Convert all points to android.Paths
        //  Log.d("save", "size is " + Integer.toString(jitteredArray.size()));
        //replace "pathsArray" -- which is the one we draw --  with our jittered paths
        pathsArray.removeAll(pathsArray);
        Log.d("points", "how many point arrays: " + Integer.toString(pointsCopy.size()));
        for (int i = 0; i < pointsCopy.size(); i++) {
            ArrayList<Point> pointPath = new ArrayList<Point>(pointsCopy.get(i));
            Path test = new Path();
            for (int j = 0; j < pointPath.size() - 1; j++) {
                Point a = new Point(pointPath.get(j));
                Point b = new Point(pointPath.get(j + 1));
                float dx = Math.abs(b.x - a.x);
                float dy = Math.abs(b.y - a.y);
                if (j == 0)
                    test.moveTo(a.x, a.y);
                Log.d("points", " " + Float.toString(a.x) + "   " + Float.toString(a.y)
                        + "    pointb " + Float.toString(b.x) + "   " + Float.toString(b.y));
                if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                    test.quadTo(a.x, a.y, (b.x + a.x) / 2.0f, (b.y + a.y) / 2.0f);
                }
            }
            pathsArray.add(test);
        }

        // Remove transparent paths
        oldPathsArray.removeAll(oldPathsArray);
     
        // Ready to draw jittered android paths

        invalidate();
        
        // Take snapshot
        this.setDrawingCacheEnabled(true);
        this.buildDrawingCache();
        bitmapCurrent = Bitmap.createScaledBitmap(this.getDrawingCache(), 480, 800, false);
        DispatchActivity.game.addFrame(bitmapCurrent);
        this.setDrawingCacheEnabled(false);
        save = false;
        
    }

    public void toggleEraser() {
        eraser = !eraser;
    }

    public void clearFrame() {
        clear = true;
        points.removeAll(points);
        pointPaths.removeAll(pointPaths);
        pathsArray.removeAll(pathsArray);
        invalidate();
        
        // This is here because if we don't have it, next path does NOT get drawn in real-time.
        pathsArray.add(mPath);
    }

    public void clear() {
        mBitmap.eraseColor(Color.GREEN);
        invalidate();
        System.gc();
    }

    public void undo() {
        Log.d("undo", "undoing");
        // TODO There is a bug in this whole code since we are technically adding more paths to pathsArray than
        // pointPaths.. so undoing all drawn paths doesn't work very well when getting to the last one. There is a duplicate
        if (pointPaths.size() > 0) {
            pointPaths.remove(pointPaths.size() - 1); 
        }
        int size = pathsArray.size();
        if(size > 0) {
            pathsArray.remove(pathsArray.size() - 1);
        }
        invalidate();
        
        //TODO add an mPath if we erased all of them to fix real time drawing.
    }

    public void nextFrame() {
        firstTime = false;
        nextFrame = true;
        Paint tmp = new Paint(mPaintTransparent);
        mPaintTransparent = new Paint(mPaint);
        invalidate();

        // Take a snapshot.
        this.setDrawingCacheEnabled(true);
        this.buildDrawingCache();
        bitmapCurrent = Bitmap.createScaledBitmap(this.getDrawingCache(), 480, 800, true);
        DispatchActivity.game.addFrame(bitmapCurrent);
        this.setDrawingCacheEnabled(false);
        
        // After the snapshot is taken, sets the paths to transparent.
        mPaintTransparent = new Paint(tmp);

    }

    public void clearAll() {
        firstTime = true;
        DispatchActivity.game.clearSessionFrames();
        oldPathsArray.removeAll(oldPathsArray);
        pathsArray.removeAll(pathsArray);
        pointPaths.removeAll(pointPaths);
        points.removeAll(points);
        invalidate();
        
        // This is here because if we don't have it, next path does NOT get drawn in real-time.
        pathsArray.add(mPath);
    }

    // Convert all points to paths
    public ArrayList<Path> toPathsArray(ArrayList<ArrayList<Point>> p) {
        ArrayList<Path> pathArray = new ArrayList<Path>();
        // For each array of points (which represents 1 path), construct a real android.Path
        for (int i = 0; i < p.size(); i++) {
            Path path = new Path(createPath(p.get(i)));
            pathArray.add(path);
        }
        // Returns an array of android.Paths
        return pathArray;
    }

    // Saves array of points to path
    public Path createPath(ArrayList<Point> p) {
        Path path = new Path();
        int div = 3;
        for (int i = 0; i < p.size() - 1; i++) {
            if (i >= 0) {
                Point point = p.get(i);

                if (i == p.size() - 1) {
                    Point prev = p.get(i - 1);
                    point.dx = ((point.x - prev.x) / div);
                    point.dy = ((point.y - prev.y) / div);

                } else if (i > 0 && i < p.size() - 1) {
                    Point next = p.get(i + 1);
                    Point prev = p.get(i - 1);
                    point.dx = ((next.x - prev.x) / div);
                    point.dy = ((next.y - prev.y) / div);
                    // mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
                    path.quadTo(point.x, point.y, (point.dx + point.x) / 2,
                            (point.dy + point.y) / 2);
                    //path.cubicTo(prev.x + prev.dx, prev.y + prev.dy, point.x - point.dx, point.y - point.dy, point.x, point.y);
                }
                if (i == 0) {
                    path.moveTo(point.x, point.y);
                    Point next = p.get(i + 1);
                    point.dx = ((next.x - point.x) / div);
                    point.dy = ((next.y - point.y) / div);
                    // path.cubicTo(point.x + point.dx, point.y + point.dy, next.x - next.dx, next.y - next.dy, next.x, next.y);
                    path.quadTo(point.x, point.y, (point.dx + point.x) / 2,
                            (point.dy + point.y) / 2);
                }

            }
        }
        return path;
    }

    public ArrayList<Point> jitterPoints2(ArrayList<Point> a) {
        ArrayList<Point> p = new ArrayList<Point>(a);
        Random rand = new Random();
        int globaloffsetX = rand.nextInt(8) - 4;
        int globaloffsetY = rand.nextInt(8) - 4;
        int randX = 0;
        int randY = 0;
        int r;
        for (int k = 0; k < p.size(); k++) {
            r = rand.nextInt(10);
            if (r > 7) {
                randX = rand.nextInt(6) - 3;
                randY = rand.nextInt(6) - 3;
            }

            Point point = p.get(k);
            point.x += globaloffsetX + randX;
            point.y += globaloffsetY + randY;
            p.set(k, point);
        }
        return p;
    }

    // Randomizes/jitters an array of points
    public ArrayList<Point> jitterPoints(ArrayList<Point> a) {
        ArrayList<Point> p = new ArrayList<Point>(a);
        Random rand = new Random();
        int globaloffsetX = rand.nextInt(10) - 5;
        int globaloffsetY = rand.nextInt(10) - 5;
        for (int k = 0; k < p.size() - 1; k++) {
            Point point = p.get(k);
            point.x += globaloffsetX;
            point.y += globaloffsetY;
            p.set(k, point);
        }
        int segment = 0; // keeps track of the first segment and last, as we don't need to gradually increment randomness, we can 
        // randomize from the start (if it's the first segment) or backwards (if it's the last segment
        int max = 70;
        int min = -70;

        double rx1 = 1; //slowly moves up to random chosen number
        double ry1 = 1;
        int rx = 0;
        int ry = 0;
        int count = 0;
        int endpoint = 0;
        int i = 0;
        if (p.size() < 2)
            return p;
        for (i = 0; i < p.size() - 2; i++) {
            int diameter = rand.nextInt(150) + 50;
            count = 0;
            Point anchor = p.get(i);
            // For each anchor iterate through points and change the ones inside its area
            for (int j = i; j < p.size() - 1; j++) {
                count++;
                Point next = p.get(j);
                double distance = Math.sqrt(Math.pow((double) (anchor.x - next.x), 2)
                        + Math.pow((double) (anchor.y - next.y), 2));
                Log.d("distance", Double.toString(distance));
                // if we get to a point that's too far away  and we have at least 10 points in our set create a new anchor
                if (distance > diameter && count > 10) {
                    anchor = next;
                    endpoint = j;
                    break;
                }
            }
            Log.d("distance", "                   eoeifjeoif" + " eowefjwoef");
            //begin randomization of this segment we just found, we know the beginning (anchor), the endpoint
            // now we can slowly add our random factor to these points and halfway through we can 
            // start decreasing this factor to end the segment at the original position.
            // So it will curve out and then in.

            // from -15 to 15
            max = 10;
            min = -10;

            rx = rand.nextInt(max - min) + min;
            ry = rand.nextInt(max - min) + min;
            int halfway = (endpoint - i) / 2;
            // factor is    randomNum / (total/2)

            //Reach whole random number halfway through the segment
            double factorX = (rx / ((endpoint - i) / 2.0) + .01); //.09 or epsilon prevents NaN errors
            double factorY = (rx / ((endpoint - i) / 2.0) + .01);
            for (int j = i; j <= endpoint; j++) {
                Point next = p.get(j);

                // If we are at the first segment, we can offset by the whole random factor from the start and
                // gradually decrement.
                if (segment <= 2 && segment > 0) { //changing to segment >= makes some paths disappear..
                    if (endpoint - (j) > halfway) {
                        next.x += rx1;
                        next.y += ry1;
                    } else { //halfway through we start going back to normal
                        next.x -= factorX;
                        next.y -= factorY;
                    }
                }

                else {
                    next.x += rx1;
                    next.y += ry1;
                    // Slowly increments to the random value for the first half of the set of points.
                    if (endpoint - j > halfway) {
                        rx1 += factorX;
                        ry1 += factorY;
                    } else {
                        rx1 -= factorX;
                        ry1 -= factorY;
                    }

                }

                p.set(j, next);
            }
            segment++; // segment processed
            i += count; // move up the anchor point
        }

        // For the last segment we go back to the last known anchor point
        i -= count;

        // last set
        rx = rand.nextInt(max - min) + min;
        ry = rand.nextInt(max - min) + min;
        int halfway = (endpoint - i) / 2;
        // factor is    randomNum / (total/2)

        double factorX = (rx / ((endpoint - i) / 2.0));
        double factorY = (rx / ((endpoint - i) / 2.0));
        for (int j = i; j <= endpoint; j++) {
            Point next = p.get(j);
            next.x += rx1;
            next.y += ry1;
            // Slowly increments to the random value for the first half of the set of points
            if (endpoint - j > halfway) {
                rx1 += factorX;
                ry1 += factorY;
            } else {
                // no normalization in the second half
                // rx1 -= factorX;
                // ry1 -= factorY;
            }
            p.set(j, next);
        }
        return p;

    }

    public class Point {
        float x, y;
        float dx, dy;

        public Point(float x, float y) {
            this.x = x;
            this.y = y;
        }

        public Point(Point p) {
            x = p.x;
            y = p.y;
            dx = p.dx;
            dy = p.dy;
        }

        @Override
        public String toString() {
            return x + ", " + y;

        }
    }
}
