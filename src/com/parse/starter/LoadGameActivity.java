package com.parse.starter;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.widget.Toast;

public class LoadGameActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_game);
    }

    @Override
    public void onResume() {
        super.onResume();
        checkGame();
    }

    public void checkGame() {
        // TODO check if we want to join a random game? So we have to query to get our data game.loadRandomGame()

        final Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    Toast.makeText(getApplicationContext(), "Starting game!", Toast.LENGTH_SHORT)
                            .show();
                    startGame();
                }
            }
        };
        DispatchActivity.game.loadGame(handler);

    }

    public void startGame() {
        final Intent intent = new Intent(LoadGameActivity.this, FlipbookActivity.class);
        startActivity(intent);
        finish();
    }

}
