package com.parse.starter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.parse.ParseAnalytics;
import com.parse.ParseUser;
import com.parse.game.Game;

/**
 * Activity which starts an intent for either the logged in (MainActivity) or
 * logged out (SignUpOrLoginActivity) activity.
 */
public class DispatchActivity extends Activity {
	// Application can only manipulate one Game object at time.
	public static Game game;
	
	// TODO just delete this and use temporary GameSession objects to build the query in HostOrJoin
	public static GameSession currentGame;

	public DispatchActivity() {
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		// track app usage
		ParseAnalytics.trackAppOpened(getIntent());

		// Check if there is current user info
		if (ParseUser.getCurrentUser() != null) {
		   
            
			// Start an intent for the logged in activity
			Log.d("parseNetwork", "user"
					+ ParseUser.getCurrentUser().getUsername());
			startActivity(new Intent(this, HostOrJoinActivity.class));
			finish();

		} else {
			// Start and intent for the logged out activity
			startActivity(new Intent(this, SignUpOrLogInActivity.class));
		}
	}

	

}
