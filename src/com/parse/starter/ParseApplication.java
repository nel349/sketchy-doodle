package com.parse.starter;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.PushService;

public class ParseApplication extends Application {

	private static SharedPreferences preferences;
	@Override
	public void onCreate() {
		super.onCreate();
		// Add your initialization code here
		ParseObject.registerSubclass(GameSession.class);
		Parse.initialize(this, "XGGFlp7fxx4NPej00d4zsV5vTSqUn3tFAlFJY8Vc", "r8AiyPYnEumgaLJLVa1XOXHsa6N4MML8Z7QPqyhZ"); 
		Log.d("parse", "start");

		// Enable to receive push
		PushService.setDefaultPushCallback(this, DispatchActivity.class);
		
	
		
	//	ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();
	    
		// If you would like all objects to be private by default, remove this line.
		defaultACL.setPublicReadAccess(true);
		defaultACL.setPublicWriteAccess(true);
		ParseACL.setDefaultACL(defaultACL, true);
		
		preferences = getSharedPreferences("com.parse.starter", Context.MODE_PRIVATE);
	}

}
