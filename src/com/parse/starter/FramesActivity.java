package com.parse.starter;

import android.app.Activity;
import android.os.Bundle;

public class  FramesActivity extends Activity {
    
    ViewingBoard view;
	
    ///tip: in eclipse you can use Ctrl + space to autocomplete
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frames_view);
        view = (ViewingBoard)findViewById(R.id.view);
    }
}