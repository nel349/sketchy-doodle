package com.parse.starter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.game.Game;

public class HostOrJoinActivity extends Activity {
    int[] framesPerTurnArr = { 5, 10, 15, 20 };
    int[] numTurnsArr = { 4, 6, 8, 10, 12 };
    int numGamesLimit = 10;
    ListView postsView;
    ListView postsView2;
    int framesPerTurn;
    int numTurns;
    int numTotalImages;
    String playerOne;
    String playerTwo;
    
    

    // Adapter for the Parse query that retrieves games in which currentUser has
    // to make a move
    private ParseQueryAdapter<GameSession> posts;

    // Adapter for the Parse query that retrieves games in which other user has
    // to make a move
    private ParseQueryAdapter<GameSession> posts2;

    EditText editText;
    Dialog loadingDialog;
    int count;
    boolean stop = false;
    RefreshThread refreshThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.host_or_join);
        count = 0;
        
        // Set up loading dialog
        loadingDialog = new Dialog(HostOrJoinActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);
        loadingDialog.setContentView(R.layout.loading_dialog);
        loadingDialog.setCanceledOnTouchOutside(false);

        
        
        
        
        // Retrieve Games where it's currentUser's turn
        ParseQueryAdapter.QueryFactory<GameSession> factory = new ParseQueryAdapter.QueryFactory<GameSession>() {
            public ParseQuery<GameSession> create() {
                // Customize query to get games where it's this user's turn
                ParseQuery<GameSession> query = DispatchActivity.currentGame.getQuery();
                query.whereEqualTo("playerTurn", ParseUser.getCurrentUser().getUsername());
                query.orderByDescending("createdAt");
                query.setLimit(numGamesLimit);
                return query;
            }
        };

     
        // Set up the query adapter
        posts = new ParseQueryAdapter<GameSession>(this, factory) {
            @Override
            public View getItemView(GameSession game, View view, ViewGroup parent) {
                if (view == null) {
                    view = View.inflate(getContext(), R.layout.anywall_post_item, null);
                }
                final String id = game.getObjectId();
                final GameSession gs = game;
                // If we click a game on this ListView, start game setup in our JoinGameActivity class
                view.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        Toast.makeText(HostOrJoinActivity.this, "clicked on the item " + id,
                                Toast.LENGTH_SHORT).show();
                        
                        // JOIN GAME
                        loadingDialog.show();
                        DispatchActivity.game = new Game(gs); //sets the Game's session to an existing one
                        Intent intent = new Intent(HostOrJoinActivity.this, LoadGameActivity.class);
                        startActivity(intent);


                    }
                });

                TextView contentView = (TextView) view.findViewById(R.id.contentView);
                TextView usernameView = (TextView) view.findViewById(R.id.usernameView);
                contentView.setText(game.getObjectId());
                usernameView.setText(game.getPlayerOne());
                usernameView.setText("Created By: " + game.getPlayerOne() + ", "
                        + game.getPlayerTwo());
                return view;
            }
        };

        // Disable automatic loading when the adapter is attached to a view.
        posts.setAutoload(false);

        // Disable pagination, we'll manage the query limit ourselves
        posts.setPaginationEnabled(false);

        // Attach the query adapter to the view
        postsView = (ListView) this.findViewById(R.id.postsView);
        postsView.setAdapter(posts);

        // Retrieve Games where it's other user's turn
        ParseQueryAdapter.QueryFactory<GameSession> factory2 = new ParseQueryAdapter.QueryFactory<GameSession>() {
            public ParseQuery<GameSession> create() {

                ParseQuery<GameSession> query = DispatchActivity.currentGame.getQuery();
                query.whereEqualTo("playerOne", ParseUser.getCurrentUser().getUsername());
                query.whereNotEqualTo("playerTurn", ParseUser.getCurrentUser().getUsername());

                ParseQuery<GameSession> query2 = DispatchActivity.currentGame.getQuery();
                query2.whereEqualTo("playerTwo", ParseUser.getCurrentUser().getUsername());
                query2.whereNotEqualTo("playerTurn", ParseUser.getCurrentUser().getUsername());

                List<ParseQuery<GameSession>> queries = new ArrayList<ParseQuery<GameSession>>();
                queries.add(query);
                queries.add(query2);

                // Gets the "or" of these two queries
                ParseQuery<GameSession> mainQuery = ParseQuery.or(queries);
                mainQuery.orderByDescending("createdAt");
                mainQuery.setLimit(numGamesLimit);

                return mainQuery;
            }
        };

        // Set up the query adapter
        posts2 = new ParseQueryAdapter<GameSession>(this, factory2) {
            @Override
            public View getItemView(GameSession game, View view, ViewGroup parent) {
                if (view == null) {
                    view = View.inflate(getContext(), R.layout.anywall_post_item, null);
                }
                final String id = game.getObjectId();
                view.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        Toast.makeText(HostOrJoinActivity.this, "clicked on the item " + id,
                                Toast.LENGTH_SHORT).show();
                    }
                });
                TextView contentView = (TextView) view.findViewById(R.id.contentView);
                TextView usernameView = (TextView) view.findViewById(R.id.usernameView);
                contentView.setText(game.getObjectId());
                usernameView.setText("Created By: " + game.getPlayerOne() + ", "
                        + game.getPlayerTwo());
                return view;
            }
        };

        // Disable automatic loading when the adapter is attached to a view.
        posts2.setAutoload(false);

        // Disable pagination, we'll manage the query limit ourselves
        posts2.setPaginationEnabled(false);

        // Attach the query adapter to the view
        postsView2 = (ListView) this.findViewById(R.id.postsView2);
        postsView2.setAdapter(posts2);

        // Load posts
        doListQuery();

        // Host new game button
        ((Button) findViewById(R.id.hostGameButton)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                showNewGameDialog();

            }
        });

        // Join game button click handler
        ((Button) findViewById(R.id.joinGameButton)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(HostOrJoinActivity.this, LoadGameActivity.class));
            }
        });

    }

    // only called if autoload is set to false.
    private void doListQuery() {
        posts.loadObjects(); // load "your turn" list
        posts2.loadObjects(); // load "their turn" list
    }

    // TODO: change to onWindowFocused? It's a better indicator that the window is visible
    @Override
    public void onResume() {
        super.onResume();
        // Unsubscribe to our channel so we don't receive app notifications while we have the app open.
        // Subscription to our channel will occur when HostOrJoinActivity goes onPause();
        Context ctx = getApplicationContext();
        PushService.unsubscribe(ctx, ParseUser.getCurrentUser().getUsername());
        ParseInstallation.getCurrentInstallation().saveInBackground();
        doListQuery();
        // Query the database and refresh our ListViews every 5 seconds
        stop = false;
        count = 0; // resume querying/refreshing list of games
        refreshThread = new RefreshThread();
        refreshThread.start();
        Log.d("parseNetwork", "resuming main activity");

        // Clear our game frame buffers if we arrive at main menu
        if(DispatchActivity.game != null)
            DispatchActivity.game.clearData();
        
        
        if(loadingDialog.isShowing()) {
            loadingDialog.dismiss();
        }
        
        
    }

    @Override
    public void onPause() {
        super.onPause();
        stop = true;

        // If we pause the application, we can receive notifications.
        if (ParseUser.getCurrentUser() != null) {
            // TODO: unregister if user logs out
            Context ctx = getApplicationContext();
            PushService.subscribe(ctx, ParseUser.getCurrentUser().getUsername(),
                    DispatchActivity.class);
            ParseInstallation.getCurrentInstallation().saveEventually();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stop = true;
    }

    private class RefreshThread extends Thread {
        @Override
        public void run() {
            try {
                while (!stop) {
                    Thread.sleep(5000);
                    // after ~1.3 minutes stop querying so we don't waste API accesses
                    // counter resets at onResume();
                    if (count < 16)
                        doListQuery();
                    Log.d("parseNetwork", "querying database");
                    count++;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean playerExists(String s) {
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("username", s);
        Log.d("parseNetwork", "verifying username availability");
        try {
            query.getFirst();
            return true;
        } catch (ParseException e1) { // if no results, player doesn't exist
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return false;
        }
    }

    public void showNewGameDialog() {
        final Dialog dlog = new Dialog(HostOrJoinActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);

        dlog.setContentView(R.layout.new_game_dialog);

        // Set the two spinners
        final Spinner spinner = (Spinner) dlog.findViewById(R.id.spinnerFramesPerTurn);
        // Create an ArrayAdapter using the string array and a
        // default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                HostOrJoinActivity.this, R.array.frames_per_turn, R.layout.spinner_layout);
        adapter.setDropDownViewResource(R.layout.spinner_layout);
        spinner.setAdapter(adapter);

        final Spinner spinner2 = (Spinner) dlog.findViewById(R.id.spinnerNumberOfTurns);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(
                HostOrJoinActivity.this, R.array.number_of_turns, R.layout.spinner_layout);
        adapter2.setDropDownViewResource(R.layout.spinner_layout);
        spinner2.setAdapter(adapter2);
        
        
        // EditText field where player 2's name will be input
        editText = (EditText) dlog.findViewById(R.id.editText1);
        editText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                playerTwo = s.toString();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        
        
        
        // Start Game Button
        ((Button) dlog.findViewById(R.id.buttonStartGame))
                .setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        // Verify playerTwo exists:
                        if(playerTwo != null) {
                            // verify playerTwo is not current user
                            if(playerTwo.equals(ParseUser.getCurrentUser().getUsername())) {
                                Toast.makeText(HostOrJoinActivity.this, "This ain't single player mode :(" ,
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if(!playerExists(playerTwo)) {
                                Toast.makeText(HostOrJoinActivity.this, "Username " + "\"" + playerTwo + "\"does not exist." ,
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }     
                        } else {
                            Toast.makeText(HostOrJoinActivity.this, "Player Two's username required to play!" ,
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }
                        
                        // Retrieve game params from dialog spinners
                        int framesPerTurn = framesPerTurnArr[spinner
                                .getSelectedItemPosition()];
                        int numTurns = numTurnsArr[spinner2.getSelectedItemPosition()];
                        Intent intent = new Intent(HostOrJoinActivity.this, LoadGameActivity.class);
                        
                        // set game params
                        playerOne = ParseUser.getCurrentUser().getUsername();
                        DispatchActivity.game = new Game(framesPerTurn, numTurns, playerOne, playerTwo, "closed"); //sets gamesession params
                        DispatchActivity.game.setFirstTurn(true);

                        // Begin loading
                        startActivity(intent);
                        dlog.dismiss();
                    }
                });
        dlog.show();

    }

}