package com.parse.starter;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class FlipbookActivity extends Activity {

    DrawingBoard drawingBoard;
    FramesActivity frameWindow;
    ProgressBar progressBar;
    GameSession game;

    // Takes care of updating the UI based on how many frames we've drawn
    public static int framesPerTurnDrawable;
    private int framesPerTurn;

    TextView txtFramesPerTurn;
    TextView txtNumTurns;
    Button buttonEndTurn;
    Button buttonNext;
    Button buttonSave;
    Context ctx;
    Dialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flipbook);

        // Set up loading dialog
        loadingDialog = new Dialog(FlipbookActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);
        loadingDialog.setContentView(R.layout.loading_dialog);
        loadingDialog.setCanceledOnTouchOutside(false);
        

        // Set Drawing Board
        drawingBoard = (DrawingBoard) findViewById(R.id.draw);

        // Retrieve Game State
        framesPerTurnDrawable = framesPerTurn = DispatchActivity.game.getFramesPerTurn();

        // Initialize GUI
        txtFramesPerTurn = (TextView) findViewById(R.id.textviewFramesPerTurn);
        txtNumTurns = (TextView) findViewById(R.id.textviewNumTurns);
        txtFramesPerTurn.setText("Frames Left: " + Integer.toString(framesPerTurnDrawable));
        txtNumTurns.setText("Turns left: " + Integer.toString(DispatchActivity.game.getNumTurns()));
        buttonEndTurn = (Button) findViewById(R.id.end_turn);
        buttonEndTurn.setEnabled(false);

        // Game could have three states: firstTurn, middle turn, or gameover
        // TODO use an enum, maybe just pass different handlers and link to the UI

        // Game over
        if (DispatchActivity.game.isOver()) {
            endTheGame();
        }

        // Middle turn
        else if (!DispatchActivity.game.isFirstTurn()) {
            showLastFrameDialog();
        }

        /******************* Begin GUI Button Listeners ****************************/

        /** Play Current Frames Drawn **/
        Button framesView = (Button) findViewById(R.id.button_frames_view);
        framesView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent ourIntent = new Intent(FlipbookActivity.this, FramesActivity.class); // set up intent based on that path/class
                startActivity(ourIntent);
            }
        });

        /** Saves the current frame but keeps and animates what was drawn **/
        buttonSave = (Button) findViewById(R.id.button_save);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                updateFrameLimit();
                drawingBoard.save();
            }
        });

        // Clear button
        Button buttonClear = (Button) findViewById(R.id.button_clear);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                drawingBoard.clearFrame();
            }
        });

        // Eraser button
        Button buttonEraser = (Button) findViewById(R.id.button_eraser);
        buttonEraser.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                drawingBoard.toggleEraser();
            }
        });

        // Undo Path Button
        Button buttonUndo = (Button) findViewById(R.id.button_undo);
        buttonUndo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                drawingBoard.undo();
            }
        });

        // Add frame
        buttonNext = (Button) findViewById(R.id.button_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                updateFrameLimit();
                drawingBoard.nextFrame();
            }
        });

        // Erases all frames drawn
        Button buttonClearAll = (Button) findViewById(R.id.button_clear_all);
        buttonClearAll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                framesPerTurnDrawable = framesPerTurn;
                updateFrameLimit();
                txtFramesPerTurn.setText("Frames Left: " + Integer.toString(framesPerTurnDrawable));
                drawingBoard.clearAll();
            }
        });
        /**
         * End Turn Button Sends images online
         */
        buttonEndTurn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                buttonEndTurn.setEnabled(false);
                loadingDialog.show();
                // Uploads images, saves game.
                final Handler handler = new Handler() {
                    public void handleMessage(Message msg) {
                        if (msg.what == 1) {
                            Toast.makeText(getApplicationContext(), "Images saved!",
                                    Toast.LENGTH_SHORT).show();
                            drawingBoard.clearAll(); //clears all point paths, etc

                            // check if we need to end the game
                            if (DispatchActivity.game.isOver()) {
                                endTheGame();
                            } else {
                                finish();
                            }
                        }
                    }
                };
                // Will send a message when all images are uploaded
                DispatchActivity.game.endTurn(handler);
            } // end onClick
        }); // End button
    }

    public void showLimitDialog() {
        Dialog dlog = new Dialog(FlipbookActivity.this, android.R.style.Theme_Holo_Light_Dialog);
        dlog.setTitle("You've reached the frame limit!");
        dlog.show();
    }

    /** Updates the UI when the frame buffer is full **/
    public void updateFrameLimit() {
        if (framesPerTurnDrawable == 1) {
            buttonNext.setEnabled(false);
            buttonSave.setEnabled(false);
            buttonEndTurn.setEnabled(true);
        } else {
            buttonNext.setEnabled(true);
            buttonSave.setEnabled(true);
            buttonEndTurn.setEnabled(false);
        }

        framesPerTurnDrawable--;
        txtFramesPerTurn.setText("Frames Left: " + Integer.toString(framesPerTurnDrawable));
    }

    // TODO:
    public void endTheGame() {
        DispatchActivity.game.clearData();
        System.gc();

        // Show end game dialog
        final Dialog dlog = new Dialog(FlipbookActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);
        dlog.setContentView(R.layout.end_game_dialog1);

        // Attach "play" all frames button
        final Button playFinalAnimButton = ((Button) dlog.findViewById(R.id.buttonPlay));
        playFinalAnimButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(FlipbookActivity.this, FramesActivity.class));
                dlog.dismiss();
                finish();
            }
        });

        // Attach progressbar
        final TextView loadingText = (TextView) dlog.findViewById(R.id.textViewLoading);
        final TextView frameNumberText = (TextView) dlog.findViewById(R.id.textViewFrameNumber);
        frameNumberText.setText(Integer.toString(DispatchActivity.game.getTotalImagesSaved())
                + " frames!");
        progressBar = (ProgressBar) dlog.findViewById(R.id.progressBar1);

        // Game class updates this handler with how many images have been downloaded
        final Handler handler2 = new Handler() {
            public void handleMessage(Message msg) {
                progressBar.setProgress(msg.what);
                // When progressbar is at 100%, show play button
                Log.d("parseNetwork", "received message: " + msg.what);
                if (msg.what == 100) {
                    Log.d("gameState", "finished downloading all images");
                    progressBar.setVisibility(View.INVISIBLE);
                    playFinalAnimButton.setVisibility(View.VISIBLE);
                    loadingText.setVisibility(View.INVISIBLE);
                }
            }
        };
        dlog.show();
        // DOWNLOAD ALL IMAGES
        DispatchActivity.game.downloadAllImages(handler2);
    }

    public void showLastFrameDialog() {
        final Dialog dlog = new Dialog(FlipbookActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);

        // Attach last frame from previous session to the dialog
        dlog.setContentView(R.layout.joined_game_dialog);
        ((ImageView) dlog.findViewById(R.id.imageView)).setImageBitmap(DispatchActivity.game
                .getLastTurnFrame());
        dlog.show();

        // "Ok" button
        ((Button) dlog.findViewById(R.id.button1)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                dlog.dismiss();
            }
        });
    }

}
